       IDENTIFICATION DIVISION. 
       PROGRAM-ID. REDEFINES3.

       ENVIRONMENT DIVISION. 
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 NanoSecs PIC 9(10).
       01 MicroSecs REDEFINES NanoSecs PIC 9999999V999.
       01 Millisecs REDEFINES NanoSecs PIC 9999V999999.
       01 Seconds REDEFINES NanoSecs PIC 9V999999999.
          
       01 EditedNum PIC Z,ZZZ,ZZZ,ZZ9.99.
       PROCEDURE DIVISION. 
       Begin.
           MOVE 1234567895 TO NanoSecs 
           MOVE NanoSecs TO EditedNum 
           DISPLAY EditedNum" Nano Secs" 
           MOVE MicroSecs TO EditedNum 
           DISPLAY EditedNum" MicroSecs"
           MOVE Millisecs TO EditedNum 
           DISPLAY EditedNum" Millisecs"
           MOVE Seconds To EditedNum 
           DISPLAY EditedNum" Seconds" 
           GOBACK.
           