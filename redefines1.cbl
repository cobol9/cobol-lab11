       IDENTIFICATION DIVISION. 
       PROGRAM-ID. REDEFINDS.

       ENVIRONMENT DIVISION. 
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 

       01 INPUT-STRING PIC X(8).
       01 WORK-AREA.
           02 F-NUM PIC 9(5) VALUE ZEROS.
           02 S-NUM PIC 99 VALUE ZEROS. 
       01 WORK-NUM REDEFINES WORK-AREA PIC 99999V99.
       01 EDIT-NUM PIC ZZ,ZZ9.99.

       PROCEDURE DIVISION. 
       Begin.
           DISPLAY "Enter a decimal number - " WITH NO ADVANCING 
           ACCEPT INPUT-STRING 
           UNSTRING INPUT-STRING DELIMITED BY "."
              INTO F-NUM, S-NUM 
           MOVE WORK-NUM TO EDIT-NUM 
           DISPLAY "Decimal Number =" EDIT-NUM 
           ADD 10 TO WORK-NUM 
           MOVE WORK-NUM TO EDIT-NUM 
           DISPLAY "Decimal Number =" EDIT-NUM
           .
           