       IDENTIFICATION DIVISION. 
       PROGRAM-ID. RENAMES1.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 STUENT-REC.
           02 STU-ID PIC 9(8) VALUE 12345678.
           02 GPA PIC 9V99 VALUE 3.25.
           02 FORE-NAME PIC X(6) VALUE "Matt".
           02 SUR-NUME PIC X(8) VALUE "Cullen".
           02 GENDER PIC X VALUE "M".
           02 PHONE-NUMBER PIC X(14) VALUE "3536120228233".
            
       66 PERSONAL-INFO RENAMES FORE-NAME THRU PHONE-NUMBER. 
       66 COLLEGE-INFO RENAMES STU-ID THRU SUR-NUME. 
       66 STUENT-NAME RENAMES FORE-NAME THRU SUR-NUME.
       01 CONTACT-INFO. 
           02 STU-NAME.
              03 STU-FORE-NAME PIC X(6).
              03 STU-SUR-NUME PIC X(8). 
           02 STU-GENDER PIC X. 
           02 STU-PHONE PIC X(14).
       66 MY-PHONE RENAMES STU-PHONE.

       PROCEDURE DIVISION. 
       Begin.
           DISPLAY "Example 1" 
           DISPLAY "All information = " STUENT-REC 
           DISPLAY "College info = " COLLEGE-INFO 
           DISPLAY "Personal Info = " PERSONAL-INFO
           DISPLAY " "
           
           DISPLAY "Example 2" 
           DISPLAY "Combined names= " STUENT-NAME
           DISPLAY " "

           MOVE PERSONAL-INFO TO CONTACT-INFO

           DISPLAY "Example 3" 
           DISPLAY "Name is " STU-NAME 
           DISPLAY "Gender is " STU-GENDER 
           DISPLAY "Phone is " STU-PHONE
           DISPLAY " "

           DISPLAY "Example 4" 
           DISPLAY "My phone is " MY-PHONE 
           GOBACK.
           
           